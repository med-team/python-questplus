python-questplus (2023.1-3) unstable; urgency=medium

  * Team upload
  * Allow more rounding errors in the tests with scipy 1.14
    (Closes: #1079788)

 -- Graham Inggs <ginggs@debian.org>  Sun, 22 Dec 2024 11:08:54 +0000

python-questplus (2023.1-2) unstable; urgency=medium

  * Team upload.

  [ Andreas Tille ]
  * Drop versioneer from d/copyright for next upload

  [ Étienne Mollier ]
  * d/control: move to autopkgtest-pkg-pybuild.
  * d/tests/: remove now unneeded manual autopkgtest.
  * d/rules: skip test_thurstone_scaling on i386.

 -- Étienne Mollier <emollier@debian.org>  Fri, 23 Aug 2024 18:03:50 +0200

python-questplus (2023.1-1) unstable; urgency=medium

  * New upstream version (makes numpy patch unnecessary)
    Closes: #1074664
  * Standards-Version: 4.7.0 (routine-update)
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)
  * Build-Depends: pybuild-plugin-pyproject

 -- Andreas Tille <tille@debian.org>  Tue, 16 Jul 2024 10:58:27 +0200

python-questplus (2019.4-6) unstable; urgency=medium

  * Team Upload.
  * Add patch to fix FTBFS with numpy (Closes: #1027202)
  * Bump Standards-Version to 4.6.2 (no changes needed)
  * Run autopkgtests for all supported versions

 -- Nilesh Patra <nilesh@debian.org>  Mon, 09 Jan 2023 14:36:54 +0530

python-questplus (2019.4-5) unstable; urgency=medium

  * Fix watch file
  * Standards-Version: 4.6.1 (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 25 Nov 2022 11:43:19 +0100

python-questplus (2019.4-4) unstable; urgency=medium

  * Fix watchfile to detect new versions on github
  * Standards-Version: 4.6.0 (routine-update)
  * debhelper-compat 13 (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 15 Oct 2021 07:02:39 +0200

python-questplus (2019.4-3) unstable; urgency=medium

  [ Sao I Kuan ]
  * Team upload
  * Add test script for autopkgtest
  * Add Testsuite: autopkgtest-pkg-python

  [ Andreas Tille ]
  * Rules-Requires-Root: no (routine-update)

 -- Sao I Kuan <saoikuan@gmail.com>  Mon, 06 Apr 2020 16:05:33 +0900

python-questplus (2019.4-2) unstable; urgency=medium

  * Add salsa-ci file (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Andreas Tille <tille@debian.org>  Sun, 23 Feb 2020 23:14:03 +0100

python-questplus (2019.4-1) unstable; urgency=medium

  * Initial release (Closes: #950984)

 -- Andreas Tille <tille@debian.org>  Sun, 09 Feb 2020 10:06:57 +0100
